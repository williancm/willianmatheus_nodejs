create database listatimes;
use listatimes;

create table estados(
	id int not null unique auto_increment primary key,
    nome varchar(255) not null unique
) ENGINE = Innodb;

create table divisoes(
	id int not null auto_increment unique primary key,
    nome varchar(255) not null unique
)ENGINE Innodb;

create table times (
	id int not null unique auto_increment primary key,
    nome varchar(255) not null unique,
    id_estado int not null,
    id_divisao int not null,
    constraint fkidestado foreign key(id_estado) references estados(id) on delete cascade,
    constraint fkiddivisao foreign key(id_divisao) references divisoes(id) on delete cascade
) ENGINE = Innodb;

insert into estados(nome) values
	('Acre'),
    ('Alagoas'),
    ('Amapá'),
    ('Amazonas'),
    ('Bahia'),
    ('Ceará'),
    ('Distrito Federal'),
    ('Espírito Santo'),
    ('Goiás'),
    ('Maranhão'),
    ('Mato Grosso'),
    ('Mato Grosso do Sul'),
    ('Minas Gerais'),
    ('Pará'),
    ('Paraíba'),
    ('Paraná'),
    ('Pernambuco'),
    ('Piauí'),
    ('Rio de Janeiro'),
    ('Rio Grande do Norte'),
    ('Rio Grande do Sul'),
    ('Rondônia'),
    ('Roraima'),
    ('Santa Catarina'),
    ('São Paulo'),
    ('Sergipe'),
    ('Tocantins');

insert into divisoes(nome) values('Primeira Divisão'), ('Segunda Divisão');

insert into times(nome, id_estado, id_divisao) values
('Atlético Paranaense', 16, 1),
('Gremio', 21, 1),
('Internacional', 21, 1),
('Bahia', 5, 2),
('Brasil de Pelotas', 21, 2);

    