var mysql = require('mysql');

function createDBConnection() {
	return mysql.createConnection({
			host: 'localhost',
			user: 'root',
			password: '',
			database: 'listatimes'
	});

};

module.exports = function(){
	return createDBConnection
};