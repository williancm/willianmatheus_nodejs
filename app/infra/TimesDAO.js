module.exports = function(){

	this.lista = function(connection, callback){
		connection.query('SELECT times.id AS id, times.nome AS times, estados.nome AS estados, divisoes.nome AS divisoes FROM times JOIN estados ON times.id_estado = estados.id JOIN divisoes ON times.id_divisao = divisoes.id', callback);
	}

	this.salva = function(connection, produto, callback){
		connection.query('insert into times set ?', produto, callback);
	}

	this.excluir = function(connection, id, callback){
		connection.query('delete from times where id = ?', id, callback);
	}

	return this;
}