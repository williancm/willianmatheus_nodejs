module.exports = function(app){
	
	app.get('/', function(req, res){
		var connection = app.infra.connectionFactory();
		var timesList = app.infra.TimesDAO;
 
		timesList.lista(connection, function(err, resposta) {
			res.render('lista', {list: resposta});
		});

		connection.end();
	});

	app.get('/serie_a', function(req, res){
		var connection = app.infra.connectionFactory();
		var divisoesList = app.infra.DivisaoDAO;
 
		divisoesList.serieA(connection, function(err, resposta) {
			res.render('serie_a/lista', {list: resposta});
		});

		connection.end();
	});

	app.get('/serie_b', function(req, res){
		var connection = app.infra.connectionFactory();
		var divisoesList = app.infra.DivisaoDAO;
 
		divisoesList.serieB(connection, function(err, resposta) {
			res.render('serie_b/lista', {list: resposta});
		});

		connection.end();
	});

	app.get('/cadastro', function(req, res){
		var connection = app.infra.connectionFactory();
		var estadosList = app.infra.EstadosDAO;

		estadosList.lista(connection, function(err, resposta) {
			res.render('form', {list: resposta});
		});

		connection.end();
	});

	app.post('/create', function(req, res){
		var time = req.body,
			connection = app.infra.connectionFactory(),
			timesDAO = app.infra.TimesDAO;		

		timesDAO.salva(connection, time, function(err, result){
			res.redirect('/');
		});
	});

}