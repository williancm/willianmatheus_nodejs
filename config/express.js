const express = require('express')
const bodyParser = require('body-parser');
const expressLayouts = require('express-ejs-layouts');
const load = require('express-load');

module.exports = function() {

	var app = express();

	app.set('view engine', 'ejs');
	app.set('views', './app/views');
	app.use(expressLayouts)           
	app.use(bodyParser.urlencoded()) 

	load('routes', {cwd: 'app'})
		.then('infra')
		.into(app);
		
	return app;
}